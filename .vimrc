set ts=4
set autoindent
set tags=/usr/src/linux-4.9.54/tags
set cscopetag
set number
"autocmd BufWritePost .vimrc so %

"Template file to show filename, author and date of creation
autocmd BufNewFile *.c :source ~/templates/author_info.c.txt

"Template file to include headers for socket files
autocmd BufNewFile *socket*.c :source ~/templates/socket_template.c

"Template file to include headers for standard c files
autocmd BufNewFile *.c :source ~/templates/template.c

"Template file to include headers for standard c++ files
autocmd BufNewFile *.cpp :source ~/templates/cpp_template.cpp

"Template file to replace the [:DATE:][:DATE_END:] marker with date and time, in a new file
autocmd BufNewFile * %substitute#\[:DATE:\]\(.\{-\}\)\[:DATE_END:\]#\=eval(submatch(1))#ge

"Template file to replace the [:NAME:][:NAME_END:] marker with the absolute path, in a new file
autocmd BufNewFile * %substitute#\[:NAME:\]\(.\{\}\)\[:END:\]#\=eval(submatch(1))#ge
