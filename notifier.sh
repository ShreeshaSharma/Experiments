#----------------------------- Notifier ------------------------------
# Aim: To send visual alerts of current time using notify-send
# to the user, every 30 (thirty minutes). And when not sending alerts
# the program must go to sleep.
#
# The use of the if-elif-else ladder is to minimise wakeup time 
# of the program and send timely alerts at the start of an hour 
# and every half hour thereon.
#
# Read the notify.log to see the times when notifier woke up.

trap "exit" INT
second=0 #$(( `date "+%S"` ))
minute=0

while [ true ]
do
	date "+%H:%M:%S %p" >> notify.log
	let second=$(( `date "+%S"` ))
	if [ $second == 0 ]
	then
		break
	elif [ $(( $second % 30 )) == 0 ]
	then
		sleep 30
	elif [ $(( $second % 15 )) == 0 ]
	then
		sleep 15
	elif [ $(( $second % 10 )) == 0 ]
	then
		sleep 10
	elif [ $(( $second % 5 )) == 0 ]
	then
		sleep 5
	else
		sleep 1
	fi
done
echo "" >> notify.log
while [ true ]
do
	date "+%H:%M:%S %p" >> notify.log
	let minute=$(( `date "+%M"` ))
	if [ $(( $minute % 30 )) == 00 ]
	then
		notify-send -i "$(echo  terminal)" "Time: $(date '+%I:%M %p')"
		"Alert sent" >> notify.log
		sleep 30m
	elif [ $(( $minute % 15 )) == 0 ]
	then
		sleep 15m
	elif [ $(( $minute % 10 )) == 0 ]
	then
		sleep 10m
	elif [ $(( $minute % 5 )) == 0 ]
	then
		sleep 5m
	elif [ $(( $minute % 2 )) == 0 ]
	then
		sleep 2m
	else
		sleep 1m
	fi
done
